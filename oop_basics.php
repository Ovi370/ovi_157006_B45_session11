<?php

class Person{

    public $name;
    public $dateOfBirth;

    public function __construct()
    {
        echo "I'm inside construct method"."<br>";
    }

    public function __destruct()
    {
        echo "good bye";
    }

    public function setName($localName)
    {
        $this->name = $localName;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }


}

class Student extends Person{
    public function doSomething(){
        echo $this->name."<br>";
    }
}

$objPerson= new Person();
$objPerson->setName("Karim");
$objPerson2= new Person();
$objPerson2->setName("Rahim");


$objStudent = new Student();
$objStudent->setName("Ovi");



echo $objStudent->doSomething()."<br>";
unset($objStudent);
echo $objPerson->name."<br>";
unset($objPerson);
echo $objPerson2->name."<br>";


?>